package com.example.project.demo.ServiceImpl;

import com.example.project.demo.Model.Challange;
import com.example.project.demo.Repository.ChallangeRepository;
import com.example.project.demo.Service.ChallangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class ChallangeServiceImpl implements ChallangeService {
    //Dan di sini untuk repository sebelum nya di panggil agar bisa berfungsi
    @Autowired
    private ChallangeRepository challangeRepository;


    @Override
    public Map<String, Object> login(Challange challange) {
        Challange masuk = challangeRepository.findByEmail(challange.getEmail()).get();
        Challange login = challangeRepository.findByPassword(challange.getPassword()).get();
        Map<String, Object> response = new HashMap<>();
        response.put("login", masuk);
        response.put("login", login);
        return response;
    }


    // Overide Seluruh Nya Agar Bisa masuk ke dalam Data Header

    @Override
    public Challange addChallange(Challange challange) {
        Challange tambah = new Challange();
        tambah.setEmail(challange.getEmail());
        tambah.setPassword(challange.getPassword());
        tambah.setUsername(challange.getUsername());
        return challangeRepository.save(tambah);
    }


    @Override
    public Challange getChallangeById(Long id) {
        return challangeRepository.findById(id).get();
    }

    //    Semua Colum yang di tambahkan Agarv terpanggil yang di dalam package model
    @Override
    public Challange editChallangeById(Long id, String email, String password, String username) {
        Challange challange = challangeRepository.findById(id).get();
        challange.setEmail(email);
        challange.setPassword(password);
        challange.setUsername(username);
        return challangeRepository.save(challange);
    }

    @Override
    public List<Challange> getAllChallange() {
        return challangeRepository.findAll();
    }

    @Override
    public void deleteChallangeById(Long id) {
        challangeRepository.deleteById(id);
    }

}